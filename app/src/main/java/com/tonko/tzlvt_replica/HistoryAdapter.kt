package com.tonko.tzlvt_replica

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tonko.tzlvt_replica.models.HistoryOperations
import kotlinx.android.synthetic.main.history_item.view.*

class HistoryAdapter(
        private val operations: ArrayList<HistoryOperations>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
                LayoutInflater
                        .from(parent.context)
                        .inflate(R.layout.history_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.tvSum.text = operations[position]?.sum.toString()
        holder.itemView.tvDate.text = "${operations[position]?.day}, ${operations[position]?.date}"
    }

    override fun getItemCount(): Int {
        return operations.size
    }

    fun removeAt(position: Int) {
        log("position = $position")
        operations.removeAt(position)
        notifyItemRemoved(position)
    }

}
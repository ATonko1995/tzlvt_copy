package com.tonko.tzlvt_replica.models

import io.realm.RealmObject

open class HistoryOperations(
        //хранение операций
        var sum: Int = 0,
        var date: String = "",
        var day: String = ""
) : RealmObject()
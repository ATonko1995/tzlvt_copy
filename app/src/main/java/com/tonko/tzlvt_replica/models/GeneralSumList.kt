package com.tonko.tzlvt_replica.models

import io.realm.RealmObject


open class GeneralSumList(
        var historyOperations: HistoryOperations? = null,
        var generalSum: GeneralSum? = null
) : RealmObject()
package com.tonko.tzlvt_replica.models

import io.realm.RealmObject

open class Day : RealmObject() {
    //модель для хранения (чтоб определять наступил ли новый день)
    var date: Int = 0
}
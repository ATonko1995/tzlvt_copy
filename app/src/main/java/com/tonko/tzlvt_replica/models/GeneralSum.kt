package com.tonko.tzlvt_replica.models

import io.realm.RealmObject


//хранение общей суммы, количества дней,
// старой суммы (в случае если пользователь тратит больше дневного бюджета поле oldSum хранит сумму в день которая была изначально,
// и сколько осталось денег в день
open class GeneralSum(
        var sum: Int = 0,
        var days: Int = 0,
        var oldSum: Int? = null,
        var remainingSum: Int? = null
) : RealmObject()
package com.tonko.tzlvt_replica.views

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.tonko.tzlvt_replica.*
import com.tonko.tzlvt_replica.models.Day
import com.tonko.tzlvt_replica.models.GeneralSum
import com.tonko.tzlvt_replica.models.GeneralSumList
import com.tonko.tzlvt_replica.models.HistoryOperations
import io.reactivex.disposables.Disposable
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.calculator.*
import kotlinx.coroutines.experimental.launch
import java.util.*


class MainActivity : AppCompatActivity() {

    var oldAmountADay = 0
    var oldGeneralAmount = 0
    var oldAmountOfDays = 0

    var initialEnter = false

    lateinit var mRealm: Realm
    lateinit var tvMoneyADayDisposable: Disposable
    lateinit var tvInputMoneyDisposable: Disposable
    lateinit var tvGeneralSumDisposable: Disposable
    var today = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        today = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)

        workingWithDB()
        workingWithNewDay()
        settingListeners()
        workingWithRx()
    }

    fun workingWithNewDay() {
        //проверяем наступает ли новый день
        // и запускаем новую активити

        val dayRealmResult = mRealm
                .where(Day::class.java)
                .findAll()
                .last()
        val day = dayRealmResult?.date


        //todo неверный расчет условий для запуска активити
        if (day != today) {
            val sumRealmResult = mRealm
                    .where(GeneralSum::class.java)
                    .findAll()
                    .last()
            val remainingMoney = sumRealmResult?.remainingSum!!
            val remainingDays = sumRealmResult.days - 1
            val oldSum = sumRealmResult.oldSum
            val generaSum = sumRealmResult.sum
            if (remainingDays > 1) {
                if (remainingMoney == 0) {
                    tvMoneyADay.text = (generaSum / remainingDays).toString()
                } else {
                    savedSomeMoney(
                            remainingMoney,
                            oldSum ?: 0,
                            remainingDays,
                            generaSum
                    )
                }
            }
            mRealm.executeTransaction {
                mRealm
                        .where(Day::class.java)
                        .findAll()
                        .deleteAllFromRealm()

                val transaction = mRealm.createObject(Day::class.java)
                transaction.date = today
            }
        }
    }

    fun savedSomeMoney(
            safeMoney: Int,
            oldSum: Int,
            newAmountOfDays: Int,
            oldGeneralSum: Int) {

        val intent = Intent(this@MainActivity, NewDayActivity::class.java)
        intent.putExtra(SAFE_MONEY, safeMoney)
        intent.putExtra(OLD_SUM, oldSum)
        intent.putExtra(NEW_AMOUNT_OF_DAYS, newAmountOfDays)
        intent.putExtra(OLD_GENERAL_SUM, oldGeneralSum)

        startActivity(intent)
    }

    fun workingWithDB() {
        mRealm = Realm.getDefaultInstance()

        val allRecords = mRealm
                .where(GeneralSumList::class.java)
                .findAll()
        //если пользователь заходит в первый раз то по умолчанию будет 8200 на 30 дней
        if (allRecords.isEmpty()) {
            initialEnter = true

            mRealm.executeTransaction {
                val newRecord = mRealm.createObject(GeneralSumList::class.java)
                val generalSum = mRealm.createObject(GeneralSum::class.java)
                generalSum.days = 30
                generalSum.sum = 8200
                generalSum.oldSum = 8200 / 30
                generalSum.remainingSum = 8200 / 30

                newRecord.generalSum = generalSum

                val savedDay = mRealm.createObject(Day::class.java)
                savedDay.date = today
            }
        }

        val lastRecordGeneralSum = allRecords.last()?.generalSum
        tvAmountOfMoney.text = lastRecordGeneralSum?.sum.toString()
        tvAmountOfDays.text = lastRecordGeneralSum?.days.toString()
        tvOldBudget.text = lastRecordGeneralSum?.oldSum.toString()

        //если никаких не проводилось выводим просто результат деления общая сумма/количество дней
        tvMoneyADay.text = when (lastRecordGeneralSum?.remainingSum) {
            null -> {
                //todo прочекать !!
                (lastRecordGeneralSum?.sum!! / lastRecordGeneralSum?.days!!).toString()
            }
            else -> {
                lastRecordGeneralSum.remainingSum.toString()
            }
        }
/*        tvMoneyADay.text =
                if (lastRecordGeneralSum?.remainingSum == null)
                //todo прочекать !!
                    (lastRecordGeneralSum?.sum ?: 1 / lastRecordGeneralSum?.days!!).toString()
                else
                    lastRecordGeneralSum.remainingSum.toString()*/

        oldAmountADay = tvMoneyADay.text.convertToInt()
        oldGeneralAmount = tvAmountOfMoney.text.convertToInt()
        oldAmountOfDays = tvAmountOfDays.text.convertToInt()

        tvDayPluralWord.text = resources
                .getQuantityText(
                        R.plurals.days_plurals,
                        tvAmountOfDays.text.convertToInt())


    }

    fun settingListeners() {
        val historyFragment = HistoryFragment()

        btnConfirm.setOnClickListener {
            if (tvInputMoney.text != "0") {
                initialEnter = false
                oldGeneralAmount = tvAmountOfMoney.text.convertToInt()
                oldAmountADay = tvMoneyADay.text.convertToInt()

                mRealm.executeTransaction {
                    //при проведении операции изменяем последнюю (и единственную) запись в бд
                    val calendar = Calendar.getInstance()
                    var inputSum = tvInputMoney.text.convertToInt()
                    val newSum = mRealm.createObject(GeneralSumList::class.java)
                    val newGeneralSum = mRealm.createObject(GeneralSum::class.java)



                    newGeneralSum?.sum = tvAmountOfMoney.text.convertToInt() + inputSum
                    newGeneralSum?.remainingSum = tvMoneyADay.text.convertToInt() + inputSum

                    val newOperation = mRealm.createObject(HistoryOperations::class.java)
                    newOperation?.day = resources.getString(R.string.today)
                    newOperation?.date = String
                            .format("%02d:%02d",
                                    calendar.get(Calendar.HOUR_OF_DAY),
                                    calendar.get(Calendar.MINUTE)
                            )

                    newOperation?.sum = inputSum




                    tvGetBackSomeMoney.text = resources.getString(R.string.get_back_money, tvInputMoney.text)
                    tvGetBackSomeMoney.visibility = View.VISIBLE

                    newSum.generalSum = newGeneralSum
                    newSum.historyOperations = newOperation
                }

                tvInputMoney.text = "0"
            }
        }

        tvHistory.setOnClickListener {

            val transaction = supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left)
            if (tvHistory.isActivated)
                transaction.remove(historyFragment)
            else
                transaction.add(R.id.flHistoryFragment, historyFragment)
            transaction.commit()

            tvHistory.isActivated = !tvHistory.isActivated
        }

        //кнопка по возвращению денег
        //todo исправить механизм
        tvGetBackSomeMoney.setOnClickListener {
            val historyOperations = mRealm
                    .where(HistoryOperations::class.java)
                    .findAll()
            var sumToGetBack: Int? = 0
            mRealm.executeTransaction {
                sumToGetBack = historyOperations.last()?.sum
                historyOperations.deleteLastFromRealm()
            }

            tvAmountOfMoney.text = (tvAmountOfMoney.text.convertToInt() + sumToGetBack!!).toString()
            tvMoneyADay.text = (tvMoneyADay.text.convertToInt() + sumToGetBack!!).toString()

            tvGetBackSomeMoney.visibility = View.GONE
        }

        btnNewDay.setOnClickListener {
            mRealm.executeTransaction {
                val day = mRealm
                        .where(Day::class.java)
                        .findAll()
                        .last()
                day?.date = 70
            }
        }
    }

    fun workingWithRx() {
        //при вводе сумму изменяем общую сумму и сумму на день
        tvInputMoneyDisposable = RxTextView
                .textChanges(tvInputMoney)
                .skipInitialValue()
                .filter {
                    changeColorAndText(
                            ContextCompat.getColor(this@MainActivity,
                                    if (tvInputMoney.text.toString() == "0") R.color.grey_input_sum
                                    else R.color.white
                            ),
                            tvInputMoney
                    )
                    tvAmountOfMoney.text.convertToInt() > 0
                }
                .subscribe {
                    val newNumber = it.convertToInt()

                    var difference = oldGeneralAmount - newNumber
                    tvAmountOfMoney.text = if (difference > 0) difference.toString() else "0"

                    difference = oldAmountADay - newNumber
                    tvMoneyADay.text = if (difference > 0) difference.toString() else "0"

                }

        changeColorAndText(tvInputMoney.text.toString() == "0" && initialEnter)

        //если сумма на день равна нулю выводим сообщение о том что изменился бюджет на день
        tvMoneyADayDisposable = RxTextView
                .textChanges(tvMoneyADay)
                .skipInitialValue()
                .doOnNext {

                }
                .subscribe {
                    changeColorAndText(tvInputMoney.text.toString() == "0" && initialEnter)

                    tvTomorrowAmountADay.text = (tvAmountOfMoney.text.convertToInt() / if (oldAmountOfDays > 1) (oldAmountOfDays - 1) else 1).toString()
                    val sumIsPositive = !(tvMoneyADay.text.convertToInt() == 0 && tvTomorrowAmountADay.text.convertToInt() != tvOldBudget.text.convertToInt())
                    showChangeOfAmountADay(
                            invisible = sumIsPositive
                    )
                }

        tvGeneralSumDisposable = RxTextView
                .textChangeEvents(tvAmountOfMoney)
                .filter {
                    wasted(false)
                    it.view().text.convertToInt() == 0
                }
                .subscribe {
                    wasted(true)
                }

    }

    fun wasted(wasted: Boolean) {
        clWasted.visibility = if (wasted) View.VISIBLE else View.GONE
        glCalculator.isEnabled = if (wasted) false else true
    }

    fun CalculatorInput(view: View) {

        val inputNumber = (view as Button).text

        if (tvInputMoney.text.toString() == "0") {
            tvInputMoney.text = inputNumber
        } else {
            tvInputMoney.append(inputNumber)
        }

    }

    fun changeColorAndText(condition: Boolean) {
        tvMoneyADay.setTextColor(
                ContextCompat.getColor(
                        this@MainActivity,
                        if (condition) R.color.grey_input_sum
                        else R.color.white
                )
        )
        tvChangeSettings.visibility = if (condition) View.VISIBLE else View.GONE
        tvMainComment.text = if (condition)
            getString(R.string.default_comment)
        else
            getString(R.string.dont_worry)
    }

    fun EraseButton(view: View) {
        if (tvInputMoney.text.length == 1 && tvInputMoney.text.toString() != "0") {
            tvInputMoney.text = "0"
        } else if (tvInputMoney.text.toString() == "0")
        else {
            tvAmountOfMoney.text = oldGeneralAmount.toString()
            tvInputMoney.text = tvInputMoney
                    .text
                    .substring(0, tvInputMoney.text.length - 1)
        }
    }

    fun changeColorAndText(
            color: Int,
            textView: TextView) {
        textView.setTextColor(color)
    }

    fun showChangeOfAmountADay(
            invisible: Boolean
    ) {
        clNewBudget.visibility = if (invisible) View.GONE else View.VISIBLE
        clComments.visibility = if (invisible) View.VISIBLE else View.GONE
        tvForTodayText.text = resources.getText(if (invisible) R.string.for_today_text else R.string.for_today_we_spend_total_budget_text)
    }

    fun onChangeSettings(view: View) {
        startActivity(
                Intent(this, SettingsActivity::class.java)
        )
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    override fun onDestroy() {
        super.onDestroy()
        launch {
            tvMoneyADayDisposable.dispose()
            tvInputMoneyDisposable.dispose()
            mRealm.close()
        }
    }
}
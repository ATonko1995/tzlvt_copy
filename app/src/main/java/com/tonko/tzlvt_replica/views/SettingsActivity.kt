package com.tonko.tzlvt_replica.views

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.format.DateUtils
import android.view.View
import android.widget.Button
import com.jakewharton.rxbinding2.widget.RxTextView
import com.tonko.tzlvt_replica.R
import com.tonko.tzlvt_replica.convertToInt
import com.tonko.tzlvt_replica.models.GeneralSum
import com.tonko.tzlvt_replica.models.GeneralSumList
import io.reactivex.disposables.Disposable
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.calculator.*
import java.util.*


class SettingsActivity : AppCompatActivity() {

    private lateinit var mRealm: Realm
    private var isGeneralSumFocused = false
    private var isDateFocused = false
    private var differenceInDays = 1
    lateinit var tvGeneralSumDisposable: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        //todo при установлении видимости в XML с этим параметром не получается работать
        calendarView.visibility = View.GONE

        workingWithDB()
        workingWithCalendar()
        settingListeners()
        workingWithRx()
    }

    private fun workingWithRx() {
        tvGeneralSumDisposable = RxTextView
                .textChanges(tvGeneralSum)
                .skipInitialValue()
                .subscribe {
                    tvSumADay.text = (tvGeneralSum.text.convertToInt() / differenceInDays).toString()
                }
    }

    private fun settingListeners() {
        clParent.setOnClickListener {
            if (isGeneralSumFocused) isGeneralSumFocused = false
            changeVisibility(isGeneralSumFocused, vBlurringView, glCalculator)
        }

        tvSave.setOnClickListener {
            mRealm.beginTransaction()
            mRealm
                    .where(GeneralSumList::class.java)
                    .findAll()
                    .deleteAllFromRealm()

            val transaction = mRealm.createObject(GeneralSumList::class.java)
            val newSum = mRealm.createObject(GeneralSum::class.java)
            newSum.sum = tvGeneralSum.text.convertToInt()
            newSum.days = differenceInDays
            newSum.oldSum = tvGeneralSum.text.convertToInt() / differenceInDays

            transaction.generalSum = newSum
            mRealm.commitTransaction()

            startActivity(
                    Intent(this@SettingsActivity, MainActivity::class.java)
            )
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
        }

        tvGeneralSum.setOnClickListener {
            isGeneralSumFocused = true
            changeVisibility(isGeneralSumFocused, vBlurringView, glCalculator)
        }

        tvSetDate.setOnClickListener {
            isDateFocused = !isDateFocused
            changeVisibility(isDateFocused, calendarView)
        }

        btnConfirm.setOnClickListener {
            changeVisibility(false, vBlurringView, glCalculator)
        }

        calendarView.setOnDateChangeListener { _, year, month, day ->
            val today = Calendar.getInstance()
            val selectedDay = Calendar.getInstance()
            selectedDay.set(year, month, day)

            differenceInDays = ((selectedDay.timeInMillis - today.timeInMillis) / (24 * 60 * 60 * 1000) + 1).toInt()
            if (differenceInDays >= 1) {
                tvSumADay.text = (tvGeneralSum.text.convertToInt() / differenceInDays).toString()

                tvSetDate.text = settingProperTimerText(
                        DateUtils.formatDateTime(this, selectedDay.timeInMillis, 0),
                        differenceInDays)
            }
        }
    }

    private fun workingWithDB() {
        mRealm = Realm.getDefaultInstance()
        val result = mRealm
                .where(GeneralSumList::class.java)
                .findAll()
                .last()
        result?.let {
            tvGeneralSum.text = it.generalSum?.sum.toString()
            differenceInDays = it.generalSum!!.days
            tvSumADay.text = (it.generalSum!!.sum / differenceInDays).toString()
        }
    }

    private fun workingWithCalendar() {
        val today = Calendar.getInstance()
        val date = Calendar.getInstance()
        date.set(
                today.get(Calendar.YEAR),
                today.get(Calendar.MONTH),
                today.get(Calendar.DATE) + differenceInDays)
        tvSetDate.text = settingProperTimerText(
                DateUtils.formatDateTime(this, date.timeInMillis, 0),
                differenceInDays)
    }

    fun CalculatorInput(view: View) {
        val inputNumber = (view as Button).text

        if (tvGeneralSum.text.toString() == "0") {
            tvGeneralSum.text = inputNumber
        } else {
            tvGeneralSum.append(inputNumber)
        }
    }

    fun EraseButton(view: View) {
        if (tvGeneralSum.text.length == 1 && tvGeneralSum.text.toString() != "0") {
            tvGeneralSum.text = "0"
        } else if (tvGeneralSum.text.toString() == "0")
        else {
            tvGeneralSum.text = tvGeneralSum
                    .text
                    .substring(0, tvGeneralSum.text.length - 1)
        }
    }

    fun changeVisibility(
            visible: Boolean,
            firstView: View,
            secondView: View? = null) {
        firstView.visibility = if (visible) View.VISIBLE else View.GONE
        secondView?.visibility = if (visible) View.VISIBLE else View.GONE
    }

    fun settingProperTimerText(date: String, difference: Int): String {
        return "До $date/на $differenceInDays ${resources.getQuantityText(R.plurals.days_plurals, difference)}"
    }

    fun onFinish(view: View) {
        finish()
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun onDestroy() {
        super.onDestroy()
        mRealm.close()
        tvGeneralSumDisposable.dispose()
    }
}

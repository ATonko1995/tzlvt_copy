package com.tonko.tzlvt_replica.views

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.tonko.tzlvt_replica.*
import com.tonko.tzlvt_replica.models.Day
import com.tonko.tzlvt_replica.models.GeneralSum
import com.tonko.tzlvt_replica.models.HistoryOperations
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_new_day.*
import java.util.*

class NewDayActivity : AppCompatActivity() {

    private var safeMoney = 0
    private var oldSum = 0
    private var newAmountOfDays = 0
    private var oldGeneralSum = 0

    private var savedSum = 0
    private var newSum = 0

    lateinit var mRealm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_day)

        //todo переработать активити чтоб оно обрабатывало колво дней == 0

        gettingSumsFromIntent()
        settingNumbersAndListeners()
        workingWithDB()
        clearOperationAtDB()
    }

    fun gettingSumsFromIntent() {
        safeMoney = intent.getIntExtra(SAFE_MONEY, 0)
        oldSum = intent.getIntExtra(OLD_SUM, 0)
        newAmountOfDays = intent.getIntExtra(NEW_AMOUNT_OF_DAYS, 0)
        oldGeneralSum = intent.getIntExtra(OLD_GENERAL_SUM, 0)

        savedSum = safeMoney + oldSum
        newSum = oldGeneralSum / newAmountOfDays
    }

    fun settingNumbersAndListeners() {
        tvYouSaved.text = resources
                .getString(
                        R.string.yesterday_you_saved,
                        safeMoney)
        tvSpendToday.text = resources
                .getString(
                        R.string.new_sum_instead_of_old,
                        savedSum, oldSum)
        tvNewBudgetText.text =
                if (newSum == oldSum)
                    resources.getString(R.string.nothing_changes)
                else
                    resources.getString(
                            R.string.new_larger_sum,
                            newSum,
                            oldSum
                    )

    }

    fun workingWithDB() {

        mRealm = Realm.getDefaultInstance()

        mRealm.executeTransaction {
            mRealm
                    .where(Day::class.java)
                    .findAll()
                    .deleteAllFromRealm()

            val newDay = mRealm.createObject(Day::class.java)
            newDay.date = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        }

        val editLastGeneralSum = mRealm
                .where(GeneralSum::class.java)
                .findAll()
                .last()

        clSpendToday.setOnClickListener {
            mRealm.executeTransaction {
                editLastGeneralSum?.days = newAmountOfDays
                editLastGeneralSum?.remainingSum = savedSum
            }
            startActivity(
                    Intent(this@NewDayActivity, MainActivity::class.java)
            )
        }

        clEnlargeBudget.setOnClickListener {
            mRealm.executeTransaction {
                editLastGeneralSum?.days = newAmountOfDays
                editLastGeneralSum?.remainingSum = newSum
            }
            startActivity(
                    Intent(this@NewDayActivity, MainActivity::class.java)
            )

        }
    }

    fun clearOperationAtDB() {
        val operations = mRealm
                .where(HistoryOperations::class.java)
                .findAll()

        //удаляем все записи которые были сделаны позавчера
        mRealm.executeTransaction {
            var operationSize = operations.size - 1

            while (operationSize >= 0) {
                if (operations[operationSize]?.day.equals(resources.getString(R.string.yesterday))) {
                    operations.deleteFromRealm(operationSize)
                } else {
                    operations[operationSize]?.day = resources.getString(R.string.yesterday)
                }
                operationSize--

            }
            mRealm.insertOrUpdate(operations)
        }
    }
}

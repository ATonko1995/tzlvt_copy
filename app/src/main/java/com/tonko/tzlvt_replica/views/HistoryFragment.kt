package com.tonko.tzlvt_replica.views


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tonko.tzlvt_replica.HistoryAdapter
import com.tonko.tzlvt_replica.R
import com.tonko.tzlvt_replica.SwipeToDeleteCallback
import com.tonko.tzlvt_replica.models.HistoryOperations
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val linearLayoutManager = LinearLayoutManager(activity)

        val mRealm = Realm.getDefaultInstance()
        linearLayoutManager.reverseLayout = true
        rvHistory.layoutManager = linearLayoutManager
        rvHistory.adapter = HistoryAdapter(
                mRealm
                        .where(HistoryOperations::class.java)
                        .findAll()
                        .toCollection(ArrayList())
        )
        val swipeHandler = object : SwipeToDeleteCallback(activity!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, p1: Int) {
                val adapter = rvHistory.adapter as HistoryAdapter
                mRealm.beginTransaction()

                //при удалении сбивается порядок операций

                val operationToDelete = mRealm
                        .where(HistoryOperations::class.java)
                        .findAll()[viewHolder.adapterPosition]
                operationToDelete?.deleteFromRealm()

                mRealm.commitTransaction()

                adapter.removeAt(viewHolder.adapterPosition)

            }

        }

        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(rvHistory)
    }

    fun deleteOperationFromRealm(
            adapter: HistoryAdapter,
            mRealm: Realm
    ) {

    }
}

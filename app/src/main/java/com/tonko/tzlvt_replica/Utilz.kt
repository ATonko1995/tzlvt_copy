package com.tonko.tzlvt_replica

import android.util.Log

fun CharSequence?.convertToInt(): Int {
    if (this == null) return 0
    return this.toString().toInt()
}

fun log(s: String) {
    Log.i("mytag", s)
}
